from expression import Expression
from variable import Variable

class Function(Expression):
    """
    Lambda abstraction consisting of a bound 
    variable and a body
    Args:
        name (Variable): bound variable in head
        body (Expression): lambda expression in body 
    """
    def __init__(self, name, body):
        assert isinstance(name, Variable)
        assert isinstance(body, Expression)
        self.name = name
        self.body = body

    @property
    def reducible(self):
        """
        a lambda abstraction can be reduced if
        the body can be reduced
        """
        return self.body.reducible

    def reduce(self):
        """
        reduce body one step
        """
        if self.body.reducible:
            return Function(self.name, self.body.reduce())
        return self


    def deep_reduce(self):
        """
        reduce body completely
        """
        return Function(self.name, self.body.deep_reduce())

    def __repr__(self):
        """
        string representation of class
        """
        return '(λ{}.{})'.format(self.name, self.body)
