### Lambda calculus

An implementation of Church's $\lambda$-calculus in python.


#### Example
```python
from lambda_calculus import λ, apply

# booleans
true = λ('x', λ('y', 'x'))
false = λ('x', λ('y', 'y'))
# if-else statement
if_then_else = λ('x', apply(λ('y', apply(λ('z', 'x'), 'y')), 'z'))

# apply conditional
# should reduce to '1', since conditional is true
apply(apply(apply(if_then_else, true), '1'), '2').print_reduce_trace()

# should reduce to '2', since conditional is false
apply(apply(apply(if_then_else, false), '1'), '(2').print_reduce_trace()

# Church numerals
zero = λ('f', λ('x', 'x'))
succ = λ('n', λ('f', λ('x', apply(apply(apply('f', 'n'), 'f'), 'x'))))

# define the Y combinator
Y = λ('f',  apply(λ('x', apply(λ('x', apply('f', 'x')), 'x')), 
                  λ('x', apply(λ('x', apply('f', 'x')), 'x'))))
# define idenity function
I = λ('x', 'x')

# Y applied to I should yield Y I = I(Y I) = Y I
apply(Y, I).print_n_reductions(6)

```