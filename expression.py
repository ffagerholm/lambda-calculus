from abc import ABC, abstractmethod


class Expression(ABC):
    """
    Abstract expression base class
    """
    def __init__(self, *args):
        super().__init__(args)
    
    @property
    @abstractmethod
    def reducible(self):
        """
        boolean determining if expression is reducible 
        """
        pass

    @abstractmethod
    def reduce(self):
        """
        reduce term one step
        """
        pass

    @abstractmethod
    def deep_reduce(self):
        """
        fully reduce the term
        caution: this may cause infinite loop
        """
        pass
    
    def print_reduce_trace(self):
        """
        recursively print reduce trace
        one reduction at a time
        """
        print(self)
        # recursive call
        if self.reducible:
            self.reduce().print_reduce_trace()

    
    def print_n_reductions(self, n):
        """
        display certain number reduce steps
        Args: 
            n (int): number of steps
        """
        if n == 0:
            return

        print(self)
        # recursive call
        if self.reducible:
            self.reduce().print_n_reductions(n - 1)
