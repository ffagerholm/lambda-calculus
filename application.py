from itertools import count
from copy import deepcopy

from expression import Expression
from function import Function
from variable import Variable

class Application(Expression):
    """
    An application
    apply a function with certain argument
    """
    counter = count()

    def __init__(self, function, argument):
        assert isinstance(function, Expression)
        assert isinstance(argument, Expression)
        self.function = function
        self.argument = argument

    @property
    def reducible(self):
        return self.function.reducible or \
               self.argument.reducible or \
               isinstance(self.function, Function)

    def reduce(self):
        """
        reduce application one step
        Returns:
            epxression (Expression):
        """
        if self.function.reducible:
            return Application(self.function.reduce(), self.argument)
        
        elif self.argument.reducible:
            return Application(self.function, self.argument.reduce())

        elif isinstance(self.function, Function):
            return self.subst(self.function.body, self.function.name, self.argument)

        return self

    def deep_reduce(self):
        """
        reduce application completely
        Returns:
            epxression (Expression):
        """
        app = self.reduce()
        if app.reducible:
            return app.deep_reduce()

        return app

    def __repr__(self):
        apply = ''
        if isinstance(self.function, Function):
            apply += '(' + str(self.function) + ')'
        else:
            apply += str(self.function)
        apply += (' ')

        if isinstance(self.argument, Variable):
            apply += str(self.argument)
        else:
            apply += '(' + str(self.argument) + ')'
        
        return apply

    def subst(self, expr, name, repl):
        """
        substitute variable in expression with replacement
        Args:
            expr (Expression):
            name (Variable):
            repl (Expression):
        Returns:
            subst_expr (Expression):
        """
        if isinstance(expr, Variable):
            if name == expr:
                return repl
            else:
                return expr
            
        elif isinstance(expr, Application):
            app = deepcopy(expr)
            return Application(
                    self.subst(app.function, name, repl),
                    self.subst(app.argument, name, repl))

        elif isinstance(expr, Function):
            fun = deepcopy(expr)

            fvs = self.free_variables(repl)
            fvs.add(name)
            
            new_name = self.unique_name(fun.name, fvs)
            new_body = self.subst(fun.body, fun.name, new_name)
            return Function(new_name, self.subst(new_body, name, repl))

        # unexpected operation
        return None

    def unique_name(self, name, used_names):
        """
        get an new unused name
        Args:
            name (Variable)
            used_names (set(str))

        Returns:
            new_name (Variable)

        """
        if name in used_names:
            return self.unique_name(Variable(name.name + "'"), used_names)
        else:
            return name

    def free_variables(self, expr):
        """
        get all free variables in the expression
        Args:
            expr (Expression)
        Returns:
            fvs set(Variables)
        """
        fvs = set()
        if isinstance(expr, Variable):
            fvs.add(expr)

        elif isinstance(expr, Application):
            app = deepcopy(expr)
            fvs = self.free_variables(app.function)
            fvs = fvs.union(self.free_variables(app.argument()))

        elif isinstance(expr, Function):
            fun = deepcopy(expr)
            fvs = self.free_variables(fun.body)
            fvs.remove(fun.name)
        
        return fvs
    
