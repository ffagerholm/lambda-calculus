"""
define lambda terms through functions with patternmatching
"""
from expression import Expression
from function import Function
from variable import Variable
from application import Application

from patternmatching import ifmatches, OfType

@ifmatches
def var(name=OfType(str)):
    """
    create a variable

    Args:
        name (str)
    Returns:
        var (Variable)
    """
    return Variable(name)


@ifmatches
def _lambda(name=OfType(str), body=OfType(str)):
    """
    create a _lambda function

    Args:
        name (str)
        body (str)
    Returns:
        fun (Function)
    """
    return Function(Variable(name), Variable(body))

@ifmatches
def _lambda(name=OfType(str), body=OfType(Expression)):
    """
    create a _lambda function

    Args:
        name (str)
        body (Expression)
    Returns:
        fun (Function)
    """
    return Function(Variable(name), body)

@ifmatches
def _lambda(name=OfType(Variable), body=OfType(Expression)):
    """
    create a _lambda function

    Args:
        name (Variable)
        body (Expression)
    Returns:
        fun (Function)
    """
    return Function(name, body)


@ifmatches
def λ(name=OfType(str), body=OfType(str)):
    """
    create a _lambda function

    Args:
        name (String)
        body (String)
    Returns:
        fun (Function)
    """
    return Function(Variable(name), Variable(body))

@ifmatches
def λ(name=OfType(str), body=OfType(Expression)):
    """
    create a _lambda function

    Args:
        name (String)
        body (Expression)
    Returns:
        fun (Function)
    """
    return Function(Variable(name), body)

@ifmatches
def λ(name=OfType(Variable), body=OfType(Expression)):
    """
    create a _lambda function
    Args:
        function (str)
        argument (str)
    Returns:
        fun (Function)
    """
    return Function(name, body)

@ifmatches
def apply(function=OfType(str), argument=OfType(str)):
    """
    apply a function with argument

    Args:
        function (str)
        argument (str)
    Returns:
        app (Application)
    """
    return Application(Variable(function), Variable(argument))

@ifmatches
def apply(function=OfType(str), argument=OfType(Expression)):
    """
    apply a function with argument

    Args:
        function (str)
        argument (Expression)

    Returns:
        app (Application)
    """
    return Application(Variable(function), argument)

@ifmatches
def apply(function=OfType(Expression), argument=OfType(str)):
    """
    apply a function with argument
    Args:
        function (Expression)
        argument (str)
    Returns:
        app (Application)
    """
    return Application(function, Variable(argument))

@ifmatches
def apply(function=OfType(Expression), argument=OfType(Expression)):
    """
    apply a function with argument
    Args:
        function (Expression)
        argument (Expression)
    Returns:
        app (Application)
    """
    return Application(function, argument)


if __name__ == '__main__':
    zero = λ('f',  λ('x', 'x'))
    succ = λ('n', λ('f', λ('x', apply('f', apply('n', apply('f', 'x'))))))

    Y = λ('f',  apply(λ('x', apply('f', apply('x', 'x'))), λ('x', apply('f', apply('x', 'x')))))
    I = λ('x', 'x')