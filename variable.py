from expression import Expression


class Variable(Expression):
    """
    Expression for a single variable
    Args:
        name (str): name of variable
    """
    def __init__(self, name):
        self.name = name

    @property
    def reducible(self):
        return False
    
    def reduce(self):
        """
        reduce term one step
        variables can not be reduced
        """
        return self

    def deep_reduce(self):
        """
        fully reduce the term
        variables can not be reduced
        """
        return self

    def __repr__(self):
        """
        string representation of class
        """
        return self.name

    def __eq__(self, other):
        if isinstance(other, Variable):
            return self.name == other.name
        else:
            raise ValueError('objects not comparable')
    
    def __hash__(self):
        return hash(repr(self))
